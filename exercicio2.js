//N = prompt("Insira um valor:")
N = 75.4;
if (N >= 0 && N <= 25) {
    console.log("Está entre 0 e 25.00000");
} else if (N > 25 && N <= 50) {
    console.log("Está entre 25.00001 e 50.00000");
} else if (N > 50 && N <= 75) {
    console.log("Está entre 50.00001 e 75.00000");
} else if (N > 75 && N <= 100) {
    console.log("Está entre 75.00001 e 100.00000");
} else {
    console.log("Fora do intervalo");
}